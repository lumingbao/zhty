//AppSecret  a77275f2f803204f9f2d461482c6c08b

import Vue from 'vue'
import App from './App'
import Constant from './common/constant.vue'

import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom', cuCustom)

Vue.config.productionTip = false
Vue.prototype.FSDCONSTANT = Constant

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
